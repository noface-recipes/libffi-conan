#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile
import os
from os import path
from conans import tools
from conans.tools import unzip, check_sha256, pythonpath

class libffiConanFile(ConanFile):
    name        = "libffi"
    version     = "3.2.1"
    description = "A portable foreign-function interface library."
    branch      = "stable"
    license     = "MIT"
    url         = "https://gitlab.com/no-face/libffi-conan"  #recipe repo url
    lib_url     = "https://sourceware.org/libffi/"           #library site

    settings    = "os", "compiler", "arch"

    options = {
        "shared" : [True, False],
        "use_pic": ["default",True, False]
    }
    default_options = (
        "shared=True",
        "use_pic=default"
    )

    ZIP_NAME                = "libffi-3.2.1.tar.gz"
    HOST_DOWNLOAD           = "sourceware.org"
    PATH_DOWNLOAD           = "pub/libffi/" + ZIP_NAME
    #FILE_URL                = BASE_URL_DOWNLOAD + "libffi-3.2.1.tar.gz"
    EXTRACTED_FOLDER_NAME   = "libffi-" + version
    FILE_SHA256             = 'd06ebb8e1d9a22d19e38d63fdb83954253f39bedc5d46232a05645685722ca37'

    build_requires = "AutotoolsHelper/0.1.0@noface/stable"

    def configure(self):
        # pure-c library
        del self.settings.compiler.libcxx

        if self.options.use_pic == "default":
            # By default enables PIC when building shared library
            self.options.use_pic = self.options.shared

    def source(self):
        tools.ftp_download(self.HOST_DOWNLOAD, self.PATH_DOWNLOAD)
        check_sha256(self.ZIP_NAME, self.FILE_SHA256)
        unzip(self.ZIP_NAME)

        os.remove(self.ZIP_NAME)

        # patch arm file that is failing
        tools.replace_in_file(
                path.join(self.EXTRACTED_FOLDER_NAME, "src", "arm", "sysv.S"),
                "stmeqia","stmiaeq")

    def build(self):
        self.configure_and_make()

    def package(self):
        install_dir = path.join(self.build_folder, "install")
        libdirs = [path.join(install_dir, 'lib'), path.join(install_dir, 'lib32')]

        # copy share dir (docs)
        self.copy("*", src=path.join(install_dir, "share"), dst="share")

        # copy libs
        for l_dir in libdirs:
            #libff puts .h in lib dir
            self.copy("*.h", dst="include", src=l_dir, keep_path=False)

            for ext in ["*.so*", "*.dll*", "*.dylib*", "*.a", "*.lib"]:
                self.copy(pattern=ext, dst="lib", src=l_dir, keep_path=False)

    def package_info(self):
        self.cpp_info.libs = ["ffi"]
        self.cpp_info.resdirs = ['share']

    ##################################################################################################

    def configure_and_make(self):

        with tools.chdir(self.EXTRACTED_FOLDER_NAME), pythonpath(self):
            from autotools_helper import Autotools

            autot = Autotools(self,
                install_dir = path.join(self.build_folder, "install"),
                shared      = self.options.shared)

            if self.options.use_pic != "default":
                autot.fpic = self.options.use_pic

            host = None

            if self.settings.os == "Android" and self.settings.arch == "x86":
                host = "i686"

            autot.configure(host=host)
            autot.build()
            autot.install()
