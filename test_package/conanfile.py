#!/usr/bin/env python
# -*- coding: utf-8 -*-

from conans import ConanFile, tools

from os import path

class TestConanFile(ConanFile):
    settings = "os", "compiler", "arch"

    build_requires = (
        "waf/0.1.1@noface/stable",
        "WafGenerator/0.0.7@noface/stable"
    )

    generators = "Waf"
    exports = "wscript"

    def imports(self):
        self.copy("*.dll", dst="bin", src="bin") # From bin to bin
        self.copy("*.dylib*", dst="bin", src="lib") # From lib to bin

    def build(self):
        self.build_path = path.join(self.build_folder, 'build')

        cmd = "waf configure build -o %s" % (self.build_path)
        self.run(cmd, cwd=self.source_folder)

    def test(self):
        exec_path = path.join(self.build_path, 'example')
        self.output.info("running test: " + exec_path)
        self.run(exec_path)
